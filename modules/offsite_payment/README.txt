Module: Commerce PayDollar Offsite
Author: Pratomo Ardianto and Quintus Leung

Description
===========
Provide Offsite payment for Commerce PayDollar.

It is recommended to enable "Commerce Paydollar Datafeed Controller" module 
to ensure transaction are safe and secured.

For more info please see README.txt on module mentioned.



Requirements
============
* Commerce PayDollar


Setup
=====
Go to Store -> Configuration -> Payment methods 
-> enable PayDollar Offsite Payment

Edit PayDollar settings and select whether you want to operate in test
or live environment, input your Merchant Id and Secure hash secret (provided
by PayDollar).


Latest Update
============
Access callback for Success, Fail, and Cancel URL now authenticated 
by using session variable.
