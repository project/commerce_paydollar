Module: Commerce PayDollar DataFeed Controller
Author: Quintus Leung

Description
===========
Provide DataFeed feature for Commerce PayDollar.

DataFeed is feature from PayDollar to provide information of 
transaction back to the site.

It is recommended to have DataFeed enabled for PayDollar 
to ensure transaction are safe and secured.

Please contact PayDollar/AsiaPay for more info.



Requirements
============
Commerce PayDollar


Setup
=====
There's nothing to be setup in the Drupal Commerce.
The only thing need to setup is from the backend of the PayDollar it self
by inserting this absolute-url below for DataFeed,
ie: http://<your-site.com>/paydollar/datafeed


Latest Update
=============
Added authentication using Secure Hash Secret 
for the DataFeed access callback URL.
