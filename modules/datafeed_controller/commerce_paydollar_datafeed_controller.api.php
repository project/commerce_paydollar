<?php
/**
 * @file
 * Hooks for the paydollar datafeed controller modules.
 */

/**
 * Hook function to re-act on data feed.
 */
function hook_commerce_paydollar_datafeed_controller($order_id, $postdata) {
  if ($postdata['successcode'] == '0') {
    // Log the datafeed status.
    watchdog('commerce_paydollar', 'PayDollar payment for %order_id success.',
      array('%order_id' => $order_id), WATCHDOG_INFO, 'admin/commmerce/orders/' . $order_id);
  }
  else {
    watchdog('commerce_paydollar', 'PayDollar payment for %order_id is failed.',
      array('%order_id' => $order_id), WATCHDOG_ERROR, 'admin/commmerce/orders/' . $order_id);
  }
}

/**
 * Payment call back function to handle the data feed.
 */
function hook_paydollar_datafeed_callback($order_id, $postdata) {
  // Update the payment transaction.
  $order = commerce_order_load($order_id);
  if ($order !== FALSE) {
    // Immediately unload to make the order available to everyone again.
    // Ref. http://drupal.org/node/1514618
    entity_get_controller('commerce_order')->resetCache(array($order->order_id));
    $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);

    $status = COMMERCE_PAYMENT_STATUS_FAILURE;
    switch ($postdata['successcode']) {
      case '0':
        $status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;

      default:
        $status = COMMERCE_PAYMENT_STATUS_FAILURE;
        break;
    }
  }
  $amount = $postdata['Amt'];
  $currency = $postdata['Cur'];
  $currency_map = array(
    '344' => 'HKD',
    '840' => 'USD',
    '702' => 'SGD',
    '156' => 'CNY',
    '392' => 'JPY',
    '901' => 'TWD',
    '036' => 'AUD',
    '036' => 'AUD',
    '978' => 'EUR',
    '826' => 'GBP',
    '124' => 'CAD',
    '446' => 'MOP',
    '608' => 'PHP',
    '764' => 'THB',
    '458' => 'MYR',
    '360' => 'IDR',
    '410' => 'KRW',
    '682' => 'SAR',
    '554' => 'NZD',
    '784' => 'AED',
    '096' => 'BND',
  );
  $currency_code = '';
  if (in_array($currency, array_keys($currency_map))) {
    $currency_code = $currency_map[$currency];
  }
  else {
    watchdog('commerce_paydollar', 'Unknown currency code from data feed record.', NULL, WATCHDOG_ERROR, 'admin/commmerce/orders/' . $order_id);
    $currency_code = commerce_default_currency();
  }
  $charge = array(
    'amount' => intval($amount) * 100,
    'currency_code' => $currency_code,
  );
  // Create payment transaction.
  $transaction = commerce_payment_transaction_new('commerce_paydollar', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = $status;
  $transaction->message = 'Name: @name';
  $transaction->message_variables = array('@name' => $postdata['Holder']);
  $transaction->remote_id = $postdata['PayRef'];
  $transaction->remote_status = '';
  commerce_payment_transaction_save($transaction);
}
