Module: Commerce PayDollar
Author: Pratomo Ardianto


Description
===========
Integrates PayDollar as a payment gateway for Drupal Commerce.

It is recommended to enable "Commerce Paydollar Datafeed Controller" 
module to ensure transaction are safe and secured.

For more info please see README.txt on module mentioned.


Requirements
============
Commerce



Installation
============
Copy the 'commerce_paydollar' module directory in to your Drupal
sites/all/modules directory as usual

Enable the module from the Modules -> Commerce PayDollar section


Setup
=====
Go to Store -> Configuration -> Payment methods -> enable PayDollar.

Edit PayDollar settings and select whether you want to operate in test
or live environment, input your Merchant Id and Secure hash secret 
(provided by PayDollar).
